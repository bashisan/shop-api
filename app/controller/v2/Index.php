<?php

namespace app\controller\v2;

use app\controller\CommonController;

class Index extends CommonController
{
    public function index(): \think\response\Json
    {
        return $this->responseInfo([], '当前为v2版本');
    }
}