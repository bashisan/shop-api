<?php

namespace app\controller\v1;

use app\controller\CommonController;

use app\middleware\v1\ManagerAction;
use app\model\v1\Category as CategoryModel;
use app\validate\v1\Ids;

class Category extends CommonController
{
    protected $middleware = [ManagerAction::class];

    /**
     * 分类列表
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getList(): \think\response\Json
    {
        $list = (new CategoryModel)->getList();
        return $this->responseData($list);
    }

    /**
     * 上线或下线 父级子级也会修改
     * @param $id
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function changeStatus($id): \think\response\Json
    {
        validate(Ids::class)->scene('single')->check(['id' => $id]);
        (new CategoryModel)->changeStatus($id);
        return $this->responseEasy();
    }
}