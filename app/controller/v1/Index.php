<?php

namespace app\controller\v1;

use app\controller\CommonController;

class Index extends CommonController
{
    public function index(): \think\response\Json
    {
        return $this->responseInfo([], '当前为v1版本');
    }
}