<?php

namespace app\controller\v1;

use app\controller\CommonController;
use app\service\v1\ManagerToken;
use app\validate\v1\Ids;
use app\model\v1\PhotoGroup as PhotoGroupModel;
use think\Request;

// 图片分类
class PhotoGroup extends CommonController
{
    /**
     * 获取列表
     * @return \think\response\Json
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getList(): \think\response\Json
    {
        $managerId = ManagerToken::getCurrentMID();
        validate(Ids::class)->scene('single')->check(['id' => $managerId]);
        $list = (new PhotoGroupModel)->getList($managerId);
        return $this->responseData($list);
    }

    /**
     * 创建分类
     * @param Request $request
     * @return \think\response\Json
     * @throws \think\Exception
     */
    public function add(Request $request): \think\response\Json
    {
        $managerId = ManagerToken::getCurrentMID();
        $name = $request->post('name', '未命名');

        $info = PhotoGroupModel::add($name, $managerId);
        return $this->responseInfo($info);
    }

    /**
     * 分类更新
     * @param Request $request
     * @return \think\response\Json
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function edit(Request $request): \think\response\Json
    {
        $id = $request->post('id');
        $name = $request->post('name', '未命名');
        $managerId = ManagerToken::getCurrentMID();
        validate(Ids::class)->scene('single')->check(['id' => $id]);

        (new PhotoGroupModel)->edit($id, $name, $managerId);
        return $this->responseEasy();
    }

    /**
     * 分类删除
     * @param $id
     * @return \think\response\Json
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function del($id): \think\response\Json
    {
        $managerId = ManagerToken::getCurrentMID();
        validate(Ids::class)->scene('single')->check(['id' => $id]);

        (new PhotoGroupModel)->del($id, $managerId);
        return $this->responseEasy();
    }
}