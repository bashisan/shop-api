<?php

namespace app\controller\v1;

use app\controller\CommonController;
use app\model\v1\Manager as ManagerModel;
use app\service\v1\ManagerToken;
use app\validate\v1\AccountLogin;
use app\validate\v1\Ids;
use app\validate\v1\PageLimit;
use app\validate\v1\PositiveInt;
use app\validate\v1\Status;
use app\validate\v1\Manager as ManagerValid;
use think\Request;

// 管理员
class Manager extends CommonController
{
    /**
     * 账号密码登录
     * @param Request $request
     * @return \think\response\Json
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function login(Request $request): \think\response\Json
    {
        $account = $request->post('account');
        $password = $request->post('password');
        validate(AccountLogin::class)->check([
            'account' => $account,
            'password' => $password
        ]);
        $managerInfo = (new ManagerModel)->infoByAccountPwd($account, $password);
        $token = (new ManagerToken)->getToken($managerInfo);
        return $this->responseInfo([
            'token' => $token
        ]);
    }

    /**
     * 清除登录
     * @return \think\response\Json
     */
    public function logout(): \think\response\Json
    {
        ManagerToken::removeToken();
        return $this->responseEasy();
    }

    /**
     * 我的信息
     * @return \think\response\Json
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function mineInfo(): \think\response\Json
    {
        $managerId = ManagerToken::getCurrentMID();
        validate(Ids::class)->scene('single')->check(['id' => $managerId]);
        $managerInfo = (new ManagerModel)->info($managerId);
        return $this->responseInfo($managerInfo->toArray());
    }

    /**
     * 分页列表
     * @param Request $request
     * @return \think\response\Json
     * @throws \think\db\exception\DbException
     */
    public function getList(Request $request): \think\response\Json
    {
        $page = $request->post('page');
        $limit = $request->post('limit');
        $roleId = $request->post('role_id');
        $status = $request->post('status');
        $keyword = $request->post('keyword');

        validate(PageLimit::class)->check(['page' => $page, 'limit' => $limit]);
        if (!is_null($roleId)) {
            validate(PositiveInt::class)->check(['positiveInt' => $roleId]);
        }
        if (!is_null($status)) {
            validate(Status::class)->check(['status' => $status]);
        }

        $managerPage = (new ManagerModel)->getList($page, $limit, $roleId, $status, $keyword);
        return $this->responsePage($managerPage);
    }

    /**
     * 管理员信息
     * @param $id
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function info($id): \think\response\Json
    {
        validate(Ids::class)->scene('single')->check(['id' => $id]);
        $managerInfo = (new ManagerModel)->info($id);
        return $this->responseInfo($managerInfo->toArray());
    }

    /**
     * 批量 冻结/恢复
     * @param Request $request
     * @return \think\response\Json
     * @throws \Exception
     */
    public function changeStatus(Request $request): \think\response\Json
    {
        $managerIds = explode(',', (string)$request->post('ids'));
        $status = (int)$request->post('status');

        validate(Ids::class)->scene('many')->check(['ids' => $managerIds]);
        validate(Status::class)->check(['status' => $status]);

        (new ManagerModel)->changeStatus($managerIds, $status);
        return $this->responseEasy();
    }

    /**
     * 批量设置角色
     * @param Request $request
     * @return \think\response\Json
     * @throws \Exception
     */
    public function setRole(Request $request): \think\response\Json
    {
        $managerIds = explode(',', (string)$request->post('ids'));
        $roleId = (int)$request->post('role_id');

        validate(Ids::class)->scene('many')->check(['ids' => $managerIds]);
        validate(Ids::class)->scene('single')->check(['id' => $roleId]);

        (new ManagerModel)->setRole($managerIds, $roleId);
        return $this->responseEasy();
    }

    /**
     * 管理员添加
     * @param Request $request
     * @return \think\response\Json
     */
    public function add(Request $request): \think\response\Json
    {
        $postForm = $request->param();
        validate(ManagerValid::class)->check($postForm);
        // 如果有id字段 删除之
        if (array_key_exists('id', $postForm)) {
            unset($postForm['id']);
        }
        (new ManagerModel)->add($postForm);
        return $this->responseEasy();
    }

    /**
     * 管理员编辑
     * @param Request $request
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function edit(Request $request): \think\response\Json
    {
        $postForm = $request->param();
        validate(Ids::class)->scene('single')->check(['id' => $postForm['id']]);
        validate(ManagerValid::class)->scene('update')->check($postForm);
        // 密码不参与更新 如果有密码字段 删除之
        if (array_key_exists('password', $postForm)) {
            unset($postForm['password']);
        }
        (new ManagerModel)->edit($postForm);
        return $this->responseEasy();
    }

    /**
     * 获取当前用户的菜单
     * @return \think\response\Json
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function authMenu(): \think\response\Json
    {
        // 基于 token 获取当前用户 id
        $managerId = ManagerToken::getCurrentMID();
        validate(Ids::class)->scene('single')->check(['id' => $managerId]);
        $authMenuList = (new ManagerModel)->authMenu($managerId);
        return $this->responseData($authMenuList);
    }
}