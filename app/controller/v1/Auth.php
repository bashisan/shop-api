<?php

namespace app\controller\v1;

use app\controller\CommonController;
use app\model\v1\Auth as AuthModel;
use app\validate\v1\Ids;
use app\validate\v1\Auth as AuthValid;
use think\Request;

class Auth extends CommonController
{
    /**
     * 权限列表
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getList(): \think\response\Json
    {
        $list = (new AuthModel)->getList();
        return $this->responseData($list);
    }

    /**
     * 权限详情
     * @param $id
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function info($id): \think\response\Json
    {
        validate(Ids::class)->scene('single')->check(['id' => $id]);
        $authInfo = (new AuthModel)->info($id);
        return $this->responseInfo($authInfo->toArray());
    }

    /**
     * 权限添加
     * @param Request $request
     * @return \think\response\Json
     */
    public function add(Request $request): \think\response\Json
    {
        $postForm = $request->param();
        validate(AuthValid::class)->check($postForm);
        // 如果有id字段 删除之
        if (array_key_exists('id', $postForm)) {
            unset($postForm['id']);
        }
        (new AuthModel)->add($postForm);
        return $this->responseEasy();
    }

    /**
     * 权限编辑
     * @param Request $request
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function edit(Request $request): \think\response\Json
    {
        $postForm = $request->param();
        validate(Ids::class)->scene('single')->check(['id' => $postForm['id']]);
        validate(AuthValid::class)->check($postForm);
        (new AuthModel)->edit($postForm);
        return $this->responseEasy();
    }

    /**
     * 上线或下线 父级子级也会修改
     * @param $id
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function changeStatus($id): \think\response\Json
    {
        validate(Ids::class)->scene('single')->check(['id' => $id]);
        (new AuthModel)->changeStatus($id);
        return $this->responseEasy();
    }

}