<?php


namespace app\controller\v1;

use app\controller\CommonController;
use app\model\v1\Role as RoleModel;
use app\validate\v1\Ids;
use app\validate\v1\Role as RoleValid;
use think\Request;

class Role extends CommonController
{
    /**
     * 角色列表
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getList(): \think\response\Json
    {
        $list = (new RoleModel)->getList();

        return $this->responseData($list);
    }

    /**
     * 角色信息
     * @param $id
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function info($id): \think\response\Json
    {
        validate(Ids::class)->scene('single')->check(['id' => $id]);
        $roleInfo = (new RoleModel)->info($id);
        return $this->responseInfo($roleInfo->toArray());
    }

    /**
     * 新增角色
     * @param Request $request
     * @return \think\response\Json
     */
    public function add(Request $request): \think\response\Json
    {
        $postForm = $request->param();
        validate(RoleValid::class)->scene('insert')->check($postForm);
        // 如果有id字段 删除之
        if (array_key_exists('id', $postForm)) {
            unset($postForm['id']);
        }
        (new RoleModel)->add($postForm);
        return $this->responseEasy();
    }

    /**
     * 角色修改
     * @param Request $request
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function edit(Request $request): \think\response\Json
    {
        $postForm = $request->param();
        validate(RoleValid::class)->check($postForm);
        (new RoleModel)->edit($postForm);
        return $this->responseEasy();
    }

    /**
     * 角色删除
     * @param $id
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function del($id): \think\response\Json
    {
        validate(Ids::class)->scene('single')->check(['id' => $id]);
        (new RoleModel)->del($id);
        return $this->responseEasy();
    }
}