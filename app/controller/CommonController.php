<?php

namespace app\controller;

use app\BaseController;
use think\App;

// 自定义的扩展基类控制器
abstract class CommonController extends BaseController
{
    /**
     * CommonController constructor.
     * @param App $app
     */
    public function __construct(App $app)
    {
        parent::__construct($app);
    }

    /**
     * 只响应状态
     * @param string $msg
     * @param int $code
     * @return \think\response\Json
     */
    protected function responseEasy(string $msg = 'success', int $code = 0): \think\response\Json
    {
        return json([
            'code'  =>  $code,
            'msg'   =>  $msg,
        ]);
    }

    /**
     * 一维数组响应
     * @param array $info
     * @param string $msg
     * @param int $code
     * @return \think\response\Json
     */
    protected function responseInfo(array $info, string $msg = 'success', int $code = 0): \think\response\Json
    {
        return json([
            'code'  =>  $code,
            'msg'   =>  $msg,
            'info'  =>  $info
        ]);
    }

    /**
     * 二维数组响应
     * @param array $data
     * @param string $msg
     * @param int $code
     * @return \think\response\Json
     */
    protected function responseData(array $data, string $msg = 'success', int $code = 0): \think\response\Json
    {
        return json([
            'code'  =>  $code,
            'msg'   =>  $msg,
            'data'  =>  $data
        ]);
    }

    /**
     * 分页数组响应
     * @param array $page
     * @param string $msg
     * @param int $code
     * @return \think\response\Json
     */
    protected function responsePage(array $page, string $msg = 'success', int $code = 0): \think\response\Json
    {
        return json([
            'code'  =>  $code,
            'msg'   =>  $msg,
            'page'  =>  $page
        ]);
    }
}