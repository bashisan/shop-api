<?php


namespace app\model\v1;


use app\BaseModel;
use app\service\v1\ManagerToken;

class Image extends BaseModel
{
    /**
     * 图片列表
     * @param int $managerId
     * @param int $page
     * @param int $limit
     * @param int|null $photoGroupId
     * @param int|null $from
     * @return array
     * @throws \think\Exception
     * @throws \think\db\exception\DbException
     */
    public function getList(int $managerId, int $page, int $limit, ?int $photoGroupId, ?int $from): array
    {
        $whereArr = [];

        !ManagerToken::isSuperManager() ? $whereArr['manager_id'] = $managerId : null;
        !is_null($photoGroupId) ? $whereArr['photo_group_id'] = $photoGroupId : null;
        !is_null($from) ? $whereArr['from'] = $from : null;

        return $this->where($whereArr)
            ->order('id', 'desc')
            ->paginate([
                'list_rows' =>  $limit,
                'page'      =>  $page
            ])->toArray();
    }

    /**
     * 图片添加
     * @param int $managerId
     * @param array $urls
     * @param int $photoGroupId
     * @param int $from
     * @return array
     * @throws \Exception
     */
    public function add(int $managerId, array $urls, int $photoGroupId, int $from): array
    {
        $createData = [];
        foreach ($urls as $key => $val) {
            if (!empty($val)) {
                $createData[$key]['url'] = $val;
                $createData[$key]['photo_group_id'] = $photoGroupId;
                $createData[$key]['manager_id'] = $managerId;
                $createData[$key]['from'] = $from;
            }
        }
        return $this->saveAll($createData)->toArray();
    }
}