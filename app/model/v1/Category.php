<?php


namespace app\model\v1;

use app\BaseModel;
use think\exception\HttpException;

class Category extends BaseModel
{
    /**
     * 一对一
     * 基于管理员表的 image_id 关联图片表的 id
     * @return \think\model\relation\HasOne
     */
    public function image(): \think\model\relation\HasOne
    {
        return $this->hasOne(Image::class, 'id', 'image_id');
    }

    /**
     * 分类列表
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getList(): array
    {
        $list = $this->with('image')->select();
        return recurrenceLevel($list->toArray(), 0);
    }

    /**
     * 分类详情
     * @param int $id
     * @return array|\think\Model
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function info(int $id) {
        $info = $this->with('image')->find($id);
        if (!$info) {
            throw new HttpException(404, '分类信息不存在');
        }
        return $info;
    }

    /**
     * 上线或下线 父级子级也会修改
     * @param int $id
     * @return bool
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function changeStatus(int $id): bool
    {
        $curCate = $this->info($id);
        if ($curCate->getAttr('status') == 1) {
            // 下线时 只需下线当前分类及子级
            // 基于当前id作为父id 查找所有子级id
            $childCateIds = $this->where('pid', $id)->column('id');
            array_push($childCateIds, $id);
            return $this->whereIn('id', $childCateIds)->save(['status' => 0]);
        } else {
            // 上线时 包括自身及父级也要上线
            // 基于当前分类的父id 递归向上查找所有的父id（含当前id）
            $parentCateIds = $this->getParentCateIds($id);
            return $this->whereIn('id', $parentCateIds)->save(['status' => 1]);
        }
    }

    /**
     * 基于当前分类的父id 递归向上查找所有的父id（含当前id）
     * @param $id
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getParentCateIds($id): array
    {
        $curCate = $this->info($id);
        $pIds = [];
        if ($curCate->getAttr('pid') != 0) {
            $pIds = $this->getParentCateIds($curCate->getAttr('pid'));
        }
        $pIds[] = $id;
        return $pIds;
    }
}