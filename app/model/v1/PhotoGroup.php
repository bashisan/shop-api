<?php

namespace app\model\v1;

use app\BaseModel;
use think\exception\ValidateException;

class PhotoGroup extends BaseModel
{
    /**
     * 获取列表
     * @param int $managerId
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getList(int $managerId): array
    {
        return $this
            ->where('manager_id', $managerId)
            ->select()
            ->toArray();
    }

    /**
     * 创建分类
     * @param string $name
     * @param int $managerId
     * @return array
     */
    public static function add(string $name, int $managerId): array
    {
        return self::create([
            'name'  =>  $name,
            'manager_id'    =>  $managerId
        ])->toArray();
    }

    /**
     * 分类更新
     * @param int $id 分类id
     * @param string $name 分类名称
     * @param int $managerId 当前管理员id
     * @return bool
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function edit(int $id, string $name, int $managerId): bool
    {
        $item = $this->find($id);
        if ($item->getAttr('manager_id') != $managerId) {
            throw new ValidateException('非法操作');
        }
        return $item->save(['name' => $name]);
    }

    /**
     * 分类删除
     * @param int $id
     * @param int $managerId
     * @return bool
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function del(int $id, int $managerId): bool
    {
        $item = $this->find($id);
        if ($item->getAttr('manager_id') != $managerId) {
            throw new ValidateException('非法操作');
        }
        return $this->delete();
    }
}