<?php

namespace app\middleware\v1;

use app\service\v1\ManagerToken;
use think\exception\HttpException;
use think\Request;

// 管理员中间件
class Manager
{
    /**
     * @param Request $request
     * @param \Closure $next
     * @return mixed
     * @throws \think\Exception
     */
    public function handle(Request $request, \Closure $next)
    {
        if (!ManagerToken::isManager()) {
            throw new HttpException(403, '你还没有一人之下万人之上的权限');
        }
        return $next($request);
    }
}