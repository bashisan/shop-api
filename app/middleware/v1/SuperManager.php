<?php

namespace app\middleware\v1;

use app\service\v1\ManagerToken;
use think\exception\HttpException;
use think\Request;

// 超管中间件
class SuperManager
{
    /**
     * @param Request $request
     * @param \Closure $next
     * @return mixed
     * @throws \think\Exception
     */
    public function handle(Request $request, \Closure $next)
    {
        if (!ManagerToken::isSuperManager()) {
            throw new HttpException(403, '你还没有至高无上的权限');
        }
        return $next($request);
    }
}