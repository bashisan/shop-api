<?php


namespace app\validate\v1;


use think\Validate;

class Url extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'	=>	['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
//        'url' =>  ['require', 'url'],
        'urls' => ['require', 'array', 'suffixWithArray']
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名'	=>	'错误信息'
     *
     * @var array
     */
    protected $message = [];

    /**
     * 文件后缀验证
     * @param $values
     * @return bool|string
     */
    protected function suffixWithArray($values) {
        if (empty($values)) {
            return 'url不能为空';
        }
        foreach ($values as $value) {
            return $this->suffix($value);
        }
        return true;
    }

    /**
     * 文件后缀验证
     * @param $value
     * @return bool|string
     */
    protected function suffix($value) {
        // 获取文件后缀
        $arr = explode('.', $value);
        if (count($arr) <= 1) {
            return '图片文件格式不正确';
        }
        $suffix = $arr[count($arr) - 1];
        // 验证后缀
        if (preg_match('/(gif|jpg|jpeg|png|webp|GIF|JPG|PNG|WEBP)/i', $suffix)) {
            return true;
        }
        return '仅支持PNG|GIF|JPG|JPEG格式图';
    }
}