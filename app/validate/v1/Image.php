<?php


namespace app\validate\v1;

use think\Validate;

class Image extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'	=>	['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'image|图片' =>  [
            'file',
            'fileSize'  => 1024*1024,
            'fileExt'   => 'jpg,jpeg,png,gif,webp',
            'fileMime'  => 'image/jpeg,image/png,image/gif'
        ]
    ];
}