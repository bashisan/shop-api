<?php

namespace app\validate\v1;

use think\Validate;

class Ids extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'	=>	['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'id'    => ['require', 'isPositiveInteger'],
        'ids'   => ['require', 'array', 'checkIDs']
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名'	=>	'错误信息'
     *
     * @var array
     */
    protected $message = [
        'ids.require'   =>  '参数不能为空',
        'ids.array'     =>  '参数格式不正确'
    ];

    /**
     * 验证场景
     * @var array
     */
    protected $scene = [
        'single'    =>  ['id'],
        'many'      =>  ['ids']
    ];

    // 自定义ID验证规则
    protected function isPositiveInteger($value) {
        if (is_numeric($value) && is_int($value + 0) && ($value + 0) > 0) {
            return true;
        } else {
            return '参数必须为正整数';
        }
    }

    // 自定义IDs验证规则 $value = [1,2,3]
    protected function checkIDs($values) {
        if (empty($values)) {
            return '参数不能为空';
        }
        foreach ($values as $id) {
            return $this->isPositiveInteger($id);
        }
        return true;
    }
}