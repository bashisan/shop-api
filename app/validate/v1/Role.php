<?php


namespace app\validate\v1;

use think\Validate;

class Role extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'	=>	['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'id|ID'    => ['require', 'integer', 'egt' => 1],
        'name|角色名称' => ['require', 'length' => '1,20'],
        'auth_ids|角色权限' => ['require', 'length' => '1,128'],
        'is_super|角色管理员' => ['require', 'integer', 'eq' => 0],
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名'	=>	'错误信息'
     *
     * @var array
     */
    protected $message = [];

    /**
     * @var array
     */
    protected $scene = [
        'insert' => ['name', 'auth_ids', 'is_super']
    ];
}