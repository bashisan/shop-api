<?php

namespace app\validate\v1;

use think\Validate;

class Auth extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'	=>	['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'pid|父级ID'    => ['require', 'integer', 'egt' => 0],
        'menu_title|菜单名称' => ['require', 'length' => '1,20'],
        'menu_path|菜单路径' => ['require', 'length' => '1,80'],
        'menu_icon|菜单图标' => ['require', 'length' => '1,48'],
        'menu_component_name|菜单组件名称' => ['require', 'length' => '1,48'],
        'menu_component_path|菜单组件路径' => ['requireWith:pid', 'length' => '1,128'],
        'menu_component_cache|菜单组件缓存' => ['require', 'integer', 'in' => '0,1'],
        'menu_props|参数props接受' => ['require', 'integer', 'in' => '0,1'],
        'menu_hidden|菜单隐藏' => ['require', 'integer', 'in' => '0,1'],
        'affix|固定tagView' => ['require', 'integer', 'in' => '0,1'],
        'breadcrumb|面包屑中显示' => ['require', 'integer', 'in' => '0,1'],
        'always_show|嵌套显示' => ['require', 'integer', 'in' => '0,1'],
        'status|菜单上线' => ['require', 'integer', 'in' => '0,1'],
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名'	=>	'错误信息'
     *
     * @var array
     */
    protected $message = [];

    /**
     * @var array
     */
    protected $scene = [

    ];
}