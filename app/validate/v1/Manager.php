<?php


namespace app\validate\v1;

use think\Validate;

class Manager extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'	=>	['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'role_id|角色ID'    => ['require', 'integer', 'egt' => 1],
        'account|账号' => ['require', 'alpha', 'length' => '5,18'],
        'password|密码' => ['require', 'alphaNum', 'length' => '6,18'],
        'checkPassword|确认密码' => ['require', 'confirm' => 'password'],
        'bind_phone|绑定手机' => ['require', 'mobile'],
        'nickname|昵称' => ['require', 'length' => '1,18'],
        'avatar|头像' => ['require', 'suffix', 'max' => 255],
        'status|状态状态' => ['require', 'integer', 'in' => '0,1'],
    ];

    /**
     * @var array
     */
    protected $scene = [
        'update' => ['role_id', 'account', 'bind_phone', 'nickname', 'avatar', 'status']
    ];

    /**
     * 文件后缀验证
     * @param $value
     * @return bool|string
     */
    protected function suffix($value) {
        // 获取文件后缀
        $arr = explode('.', $value);
        if (count($arr) <= 1) {
            return '图片文件格式不正确';
        }
        $suffix = $arr[count($arr) - 1];
        // 验证后缀
        if (preg_match('/^(PNG|GIF|JPG|JPEG)$/i', $suffix)) {
            return true;
        }
        return '图片文件格式不正确';
    }
}