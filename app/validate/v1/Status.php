<?php


namespace app\validate\v1;

use think\Validate;

// 0 或 1 的状态
class Status extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'	=>	['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'status' =>  ['require', 'integer', 'in' => '0,1'],
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名'	=>	'错误信息'
     *
     * @var array
     */
    protected $message = [];
}