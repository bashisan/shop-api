<?php

declare (strict_types = 1);

namespace app\validate\v1;

use think\Validate;

class AccountLogin extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'	=>	['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'account|账号' => ['require', 'length' => '5,12'],
        'password|密码' => ['require', 'length' => '6,18']
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名'	=>	'错误信息'
     *
     * @var array
     */
    protected $message = [];
}