<?php
// 应用公共文件

/**
 * 生成随机字符串
 * @param int $len
 * @return string|null
 */
function getRandChar(int $len = 32): ?string
{
    $str = null;
    $strPol = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
    $max = strlen($strPol) - 1;
    for ($i = 0; $i < $len; $i++) {
        $str .= $strPol[rand(0, $max)];
    }
    return $str;
}

/**
 * 手机号加密
 * 15904878331 => 159****8331
 * @param string $phone 源字符串
 * @param string $replace 替换的字符
 * @param int $offset 最测偏移位置
 * @param int $length 替换长度
 * @return string|string[]
 */
function encryptPhone(string $phone, string $replace, int $offset = 3, int $length = 4) {
    return substr_replace($phone, $replace, $offset, $length);
}

/**
 * 递归组装无限极分类、权限
 * @param array $arr 原始数组
 * @param int $pid 父id
 * @return array
 */
function recurrenceLevel(array $arr, int $pid = 0): array
{
    $result = [];
    foreach ($arr as $v) {
        if ($pid == $v['pid']) {
            $v['children'] = recurrenceLevel($arr, $v['id']);
            $result[] = $v;
        }
    }
    return $result;
}