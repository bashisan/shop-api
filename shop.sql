/*
Navicat MySQL Data Transfer

Source Server         : 本地
Source Server Version : 50726
Source Host           : localhost:3306
Source Database       : shop

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2021-05-31 19:46:00
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for auth
-- ----------------------------
DROP TABLE IF EXISTS `auth`;
CREATE TABLE `auth` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `pid` int(10) NOT NULL DEFAULT '0' COMMENT '父id',
  `menu_title` varchar(20) NOT NULL DEFAULT '' COMMENT '菜单标题',
  `menu_path` varchar(80) NOT NULL DEFAULT '' COMMENT '菜单路径 如：/goods',
  `menu_icon` varchar(48) DEFAULT '' COMMENT '菜单图标',
  `menu_redirect` varchar(48) DEFAULT '' COMMENT '菜单重定向路径 子路径为空 如：/goods/list',
  `menu_component_name` varchar(48) NOT NULL DEFAULT '' COMMENT '菜单组件名称 如：Goods',
  `menu_component_path` varchar(128) NOT NULL DEFAULT '' COMMENT '菜单组件路径 顶级路由为空 如：/views/goods/list',
  `menu_component_cache` tinyint(1) DEFAULT '1' COMMENT '菜单组件是否缓存 1是 0否',
  `menu_active` varchar(128) DEFAULT '' COMMENT '当不在菜单中显示时要高亮的菜单 如：/goods/list',
  `menu_props` tinyint(1) DEFAULT '0' COMMENT '参数是否props接受 1是 0否',
  `menu_hidden` tinyint(1) DEFAULT '0' COMMENT '菜单是否隐藏 1是 0否',
  `level` tinyint(4) NOT NULL DEFAULT '0' COMMENT '层级',
  `affix` tinyint(1) DEFAULT '0' COMMENT '固定tagView不可移除 0不固定 1固定',
  `breadcrumb` tinyint(1) DEFAULT '1' COMMENT '是否在面包屑中显示 0不显示 1显示',
  `always_show` tinyint(1) DEFAULT '1' COMMENT '是否显示根目录（只有一个子路由时） 0不显示 1显示',
  `status` tinyint(1) DEFAULT '1' COMMENT '是否上线 0下线 1上线',
  `actions` varchar(255) DEFAULT '' COMMENT '可操作方法 如：list、add、edit',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COMMENT='权限表';

-- ----------------------------
-- Records of auth
-- ----------------------------
INSERT INTO `auth` VALUES ('1', '0', '商品管理', '/goods', 'el-icon-s-help', '/goods/list', 'Goods', '', '1', '', '0', '0', '0', '0', '1', '1', '1', '');
INSERT INTO `auth` VALUES ('2', '0', '分类管理', '/category', 'table', '/category/list', 'Category', '', '1', '', '0', '0', '0', '0', '1', '1', '1', '');
INSERT INTO `auth` VALUES ('3', '0', '权限管理', '/auth', 'example', '/auth/list', 'Auth', '', '1', '', '0', '0', '0', '0', '1', '1', '1', '');
INSERT INTO `auth` VALUES ('4', '0', '订单管理', '/order', 'el-icon-s-help', '/order/list', 'Order', '', '1', '', '0', '0', '0', '0', '1', '1', '1', '');
INSERT INTO `auth` VALUES ('5', '1', '商品列表', 'list', 'el-icon-s-help', '', 'GoodsList', '/goods/list', '1', '', '0', '0', '1', '0', '1', '1', '1', '');
INSERT INTO `auth` VALUES ('6', '1', '商品添加', 'create', 'el-icon-s-help', '', 'GoodsCreate', '/goods/create', '1', '', '0', '0', '1', '0', '1', '1', '1', '');
INSERT INTO `auth` VALUES ('7', '2', '分类列表', 'list', 'el-icon-s-help', '', 'CategoryList', '/category/list', '1', '', '0', '0', '1', '0', '1', '1', '1', '');
INSERT INTO `auth` VALUES ('8', '2', '分类添加', 'create', 'el-icon-s-help', '', 'CategoryCreate', '/category/create', '1', '', '0', '0', '1', '0', '1', '1', '1', '');
INSERT INTO `auth` VALUES ('9', '3', '权限列表', 'list', 'el-icon-s-help', '', 'AuthList', '/auth/list', '1', '', '0', '0', '1', '0', '1', '1', '1', '');
INSERT INTO `auth` VALUES ('10', '3', '权限添加', 'create', 'el-icon-s-help', '', 'AuthCreate', '/auth/create', '1', '', '0', '0', '1', '0', '1', '1', '1', '');
INSERT INTO `auth` VALUES ('11', '4', '订单列表', 'list', 'el-icon-s-help', '', 'OrderList', '/order/list', '1', '', '0', '0', '1', '1', '1', '1', '1', '');
INSERT INTO `auth` VALUES ('12', '4', '订单添加', 'create', 'el-icon-s-help', '', 'OrderCreate', '/order/create', '1', '', '0', '0', '1', '0', '1', '1', '1', '');

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `pid` int(10) NOT NULL DEFAULT '0' COMMENT '父id',
  `name` varchar(20) NOT NULL COMMENT '分类名称',
  `image_id` int(10) DEFAULT '0' COMMENT '外键 image表id',
  `display_order` tinyint(3) unsigned DEFAULT '0' COMMENT '排序',
  `status` tinyint(1) DEFAULT '1' COMMENT '是否上线 0下线 1上线',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COMMENT='分类表';

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES ('1', '0', '女装', '0', '0', '1');
INSERT INTO `category` VALUES ('2', '0', '男装', '0', '0', '1');
INSERT INTO `category` VALUES ('3', '0', '鞋子', '0', '0', '1');
INSERT INTO `category` VALUES ('4', '1', '女裙', '0', '1', '1');
INSERT INTO `category` VALUES ('5', '1', '上衣', '0', '1', '1');
INSERT INTO `category` VALUES ('6', '1', '裤子', '0', '1', '1');
INSERT INTO `category` VALUES ('7', '1', '内裤', '0', '1', '1');
INSERT INTO `category` VALUES ('8', '1', '文胸', '0', '1', '1');
INSERT INTO `category` VALUES ('9', '2', '裤子', '0', '1', '1');
INSERT INTO `category` VALUES ('10', '2', '上衣', '0', '1', '1');
INSERT INTO `category` VALUES ('11', '3', '女鞋', '0', '1', '1');
INSERT INTO `category` VALUES ('12', '3', '男鞋', '0', '1', '1');
INSERT INTO `category` VALUES ('13', '11', '豆豆鞋', '0', '2', '1');
INSERT INTO `category` VALUES ('14', '11', '高跟鞋', '0', '2', '1');
INSERT INTO `category` VALUES ('15', '12', '工装靴', '0', '2', '1');
INSERT INTO `category` VALUES ('16', '12', '马丁靴', '0', '2', '1');

-- ----------------------------
-- Table structure for image
-- ----------------------------
DROP TABLE IF EXISTS `image`;
CREATE TABLE `image` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) DEFAULT NULL COMMENT '路径',
  `photo_group_id` int(10) NOT NULL DEFAULT '0' COMMENT '外键 图片分类表id',
  `manager_id` int(10) NOT NULL DEFAULT '0' COMMENT '外键 管理员表id',
  `from` tinyint(1) NOT NULL DEFAULT '0' COMMENT '存储位置 0本地 1云存储',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='分类表';

-- ----------------------------
-- Records of image
-- ----------------------------
INSERT INTO `image` VALUES ('1', 'https://i1.hdslb.com/bfs/face/c198d0deb03cfc6aead8ede8b8feb8669276d1f4.jpg', '1', '1', '1');

-- ----------------------------
-- Table structure for photo_group
-- ----------------------------
DROP TABLE IF EXISTS `photo_group`;
CREATE TABLE `photo_group` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL COMMENT '分类名称',
  `manager_id` int(10) NOT NULL DEFAULT '0' COMMENT '外键 管理员表id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='图片分类表';

-- ----------------------------
-- Records of photo_group
-- ----------------------------
INSERT INTO `photo_group` VALUES ('1', '封面图', '1');
INSERT INTO `photo_group` VALUES ('2', '内容图', '1');
INSERT INTO `photo_group` VALUES ('3', '分类图', '2');

-- ----------------------------
-- Table structure for manager
-- ----------------------------
DROP TABLE IF EXISTS `manager`;
CREATE TABLE `manager` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `role_id` tinyint(3) NOT NULL DEFAULT '0' COMMENT '角色id',
  `account` varchar(32) NOT NULL COMMENT '账号',
  `password` char(32) NOT NULL COMMENT '密码',
  `bind_phone` varchar(32) NOT NULL COMMENT '绑定手机',
  `nickname` varchar(32) NOT NULL COMMENT '昵称',
  `image_id` int(10) NOT NULL DEFAULT '0' COMMENT '头像 图片表id',
  `status` tinyint(1) DEFAULT '0' COMMENT '是否冻结 1是 0否',
  `login_count` int(6) DEFAULT '0' COMMENT '登录次数',
  `login_ip` char(15) DEFAULT '0.0.0.0' COMMENT '最后登录ip',
  `login_time` datetime DEFAULT NULL COMMENT '最后登录时间',
  `create_time` datetime DEFAULT NULL COMMENT '注册时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='管理员表';

-- ----------------------------
-- Records of manager
-- ----------------------------
INSERT INTO `manager` VALUES ('1', '1', 'admin', 'e10adc3949ba59abbe56e057f20f883e', '15904878331', 'XBSS', '16', '0', '0', '192.168.3.1', '2021-05-04 19:15:10', '2020-09-04 19:15:10');
INSERT INTO `manager` VALUES ('2', '2', 'tom333', 'e10adc3949ba59abbe56e057f20f883e', '15904878332', '小丑', '14', '0', '0', '192.168.3.1', '2021-05-04 19:15:10', '2020-09-04 19:15:10');
INSERT INTO `manager` VALUES ('3', '3', 'lincoln', 'e10adc3949ba59abbe56e057f20f883e', '15904878333', '丘比特', '13', '0', '0', '192.168.3.1', '2021-05-04 19:15:10', '2020-09-04 19:15:10');

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL COMMENT '角色名称',
  `auth_ids` varchar(128) NOT NULL DEFAULT '' COMMENT '权限ids，如：1,2,3',
  `is_super` tinyint(1) DEFAULT '0' COMMENT '是否是超管 0否 1是',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='角色表';

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES ('1', '超管', '', '1');
INSERT INTO `role` VALUES ('2', '运营', '1,2,5,6,7,8', '0');
INSERT INTO `role` VALUES ('3', '客服', '1,4,5,11,12', '0');
