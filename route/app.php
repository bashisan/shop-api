<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use think\facade\Route;

Route::group('api/:version', function () {
    Route::get('index', ':version.Index/index');
    // 管理员
    Route::group('manager', function () {
        Route::post('login', ':version.Manager/login');

        Route::group(function () {
            Route::get('logout', ':version.Manager/logout');
            Route::get('mineInfo', ':version.Manager/mineInfo');
            Route::get('authMenu', ':version.Manager/authMenu');
        })->middleware('ManagerAuth');

        Route::group(function () {
            Route::post('list', ':version.Manager/getList');
            Route::get('info/:id', ':version.Manager/info');
            Route::post('changeStatus', ':version.Manager/changeStatus');
            Route::post('setRole', ':version.Manager/setRole');
            Route::post('create', ':version.Manager/add');
            Route::post('update', ':version.Manager/edit');
        })->middleware('SuperManagerAuth');
    });
    // 角色
    Route::group('role', function () {
        Route::get('list', ':version.Role/getList');
        Route::get('info/:id', ':version.Role/info');
        Route::post('create', ':version.Role/add');
        Route::post('update', ':version.Role/edit');
        Route::get('delete/:id', ':version.Role/del');
    })->middleware('SuperManagerAuth');
    // 权限
    Route::group('auth', function () {
        Route::get('list', ':version.Auth/getList');
        Route::get('info/:id', ':version.Auth/info');
        Route::post('create', ':version.Auth/add');
        Route::post('update', ':version.Auth/edit');
        Route::get('changeStatus/:id', ':version.Auth/changeStatus');
    })->middleware('SuperManagerAuth');
    // 图片分类
    Route::group('photoGroup', function () {
        Route::get('list', ':version.PhotoGroup/getList');
        Route::post('create', ':version.PhotoGroup/add');
        Route::post('update', ':version.PhotoGroup/edit');
        Route::get('delete/:id', ':version.PhotoGroup/del');
    })->middleware('ManagerAuth');
    // 图片
    Route::group('image', function () {
        Route::post('list', ':version.Image/getList');
        Route::post('create', ':version.Image/add');
    })->middleware('ManagerAuth');
    // 分类
    Route::group('category', function () {
        Route::get('list', ':version.Category/getList');
        Route::get('changeStatus/:id', ':version.Category/changeStatus');
    });
    // 公共
    Route::group('common', function () {
        Route::group('upload', function () {
            Route::rule('single', ':version.Upload/single');
            Route::rule('multiple', ':version.Upload/multiple');
        });
    });
});